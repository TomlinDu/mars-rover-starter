# Daily Report (2023/07/13) 

## O(Objective): What did we learn today? What activities did you do? What scenes have impressed you? 
o  This morning, we still showed yesterday's homework, and practiced the object-oriented programming we learned yesterday. I learned some new ideas and some methods of refactoring code through the coding shared by everyone. 

o  Then we learned test-driven development (TDD), TDD has three main points, namely given, when and then, these three points can be clarified to clearly describe the task of test.
TDD can help us identify problems earlier and reduce maintenance costs later.

o  We have done a lot of TDD related exercises today. Different from before, today's exercises require commit after test is written, and commit after function is completed and tested, which requires multiple commit comparisons and is rather tedious.

##  R(Reflective): Please use one word to express your feelings about today’s class. 
o	Good. 

##  I(Interpretive): What do you think about this? What was the most meaningful aspect of this activity? 
o   Today's study is less stressful than the last few days, I can better understand and try my best to keep up with the teacher's progress, and I also learned a lot of new basic knowledge of java. Thank you for your patience to answer my questions.

##  D(Decisional): Where do you most want to apply what you have learned today? What changes will you make? 
o	Through TDD, I can better clarify task requirements, and it is also a good programming method from the perspective of test, which can avoid slowly finding problems from a large number of codes and easily and quickly find problems that need to be modified.