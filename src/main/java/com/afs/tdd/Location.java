package com.afs.tdd;

public class Location {
    private int coordinateX;
    private int coordinateY;
    private Direction direction;

    public Location(int coordinateX, int coordinateY, Direction direction) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.direction = direction;
    }

    public int getCoordinateX() {
        return coordinateX;
    }
    public int getCoordinateY() {
        return coordinateY;
    }
    public Direction getDirection() {
        return direction;
    }
    public void addCoordinateY() {
        this.coordinateY++;
    }
    public void addCoordinateX() {
        this.coordinateX++;
    }
    public void deductCoordinateY() {
        this.coordinateY--;
    }
    public void deductCoordinateX() {
        this.coordinateX--;
    }
    public void changeDirection(Direction direction){
        this.direction=direction;
    }
}
