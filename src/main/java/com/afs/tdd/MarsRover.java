package com.afs.tdd;

public class MarsRover {
    private Location location;

    public Location getLocation() {
        return location;
    }

    public MarsRover(Location locationInitial) {
        this.location = locationInitial;
    }

    public Location executeCommand(Command command) {
        Location locationResult = location;
        switch (command){
            case Move:
                move();
                break;
            case TurnLeft:
                turnLeft();
                break;
            case TurnRight:
                turnRight();
                break;
        }
        return locationResult;
    }
    private void move() {
        switch (this.location.getDirection()){
            case North:
                this.location.addCoordinateY();
                break;
            case East:
                this.location.addCoordinateX();
                break;
            case South:
                this.location.deductCoordinateY();
                break;
            case West:
                this.location.deductCoordinateX();
                break;
        }
    }
    private void turnLeft(){
        switch (this.location.getDirection()){
            case North:
                this.location.changeDirection(Direction.West);
                break;
            case East:
                this.location.changeDirection(Direction.North);
                break;
            case South:
                this.location.changeDirection(Direction.East);
                break;
            case West:
                this.location.changeDirection(Direction.South);
                break;
        }
    }
    private void turnRight() {
        switch (this.location.getDirection()){
            case North:
                this.location.changeDirection(Direction.East);
                break;
            case East:
                this.location.changeDirection(Direction.South);
                break;
            case South:
                this.location.changeDirection(Direction.West);
                break;
            case West:
                this.location.changeDirection(Direction.North);
                break;
        }
    }

}
