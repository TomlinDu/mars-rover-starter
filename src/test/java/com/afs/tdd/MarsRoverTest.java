package com.afs.tdd;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MarsRoverTest {
    @Test
    void should_change_to_0_1_North_when_executeCommand_given_location_0_0_N_and_command_move(){
        //Given
        Location locationInitial = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_N_and_command_TurnLeft(){
        //Given
        Location locationInitial = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandTurnLeft = Command.TurnLeft;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandTurnLeft);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_N_and_command_TurnRight(){
        //Given
        Location locationInitial = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandTurnRight = Command.TurnRight;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandTurnRight);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_1_0_East_when_executeCommand_given_location_0_0_E_and_command_move(){
        //Given
        Location locationInitial = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(1,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_E_and_command_TurnLeft(){
        //Given
        Location locationInitial = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandTurnLeft = Command.TurnLeft;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandTurnLeft);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_E_and_command_TurnRight(){
        //Given
        Location locationInitial = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandTurnRight = Command.TurnRight;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandTurnRight);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_minus1_South_when_executeCommand_given_location_0_0_S_and_command_move(){
        //Given
        Location locationInitial = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(-1,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_S_and_command_TurnLeft(){
        //Given
        Location locationInitial = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandTurnLeft = Command.TurnLeft;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandTurnLeft);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_S_and_command_TurnRight(){
        //Given
        Location locationInitial = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandTurnRight = Command.TurnRight;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandTurnRight);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_minus1_0_West_when_executeCommand_given_location_0_0_W_and_command_move(){
        //Given
        Location locationInitial = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        //Then
        Assertions.assertEquals(-1,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_W_and_command_TurnLeft(){
        //Given
        Location locationInitial = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandTurnLeft = Command.TurnLeft;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandTurnLeft);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_W_and_command_TurnRight(){
        //Given
        Location locationInitial = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandTurnRight = Command.TurnRight;
        //When
        Location locationAfterMove = marsRover.executeCommand(commandTurnRight);
        //Then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North,locationAfterMove.getDirection());
    }

}

